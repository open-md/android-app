package com.openmd.openmd;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Requests requests = new Requests(this);
    Button btnSearch;
    TextInputEditText searchInput;
    private static final String TAG = "MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        searchInput = findViewById(R.id.main_search_input);
        btnSearch = findViewById(R.id.main_btn_search);

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String searchWord = searchInput.getText().toString();
                    requests.search(searchWord, new Requests.VolleyCallback() {
                        @Override
                        public void onSuccess(String resp) {
                            Intent intent = new Intent(MainActivity.this, MapActivity.class);
                            intent.putExtra("EXTRA_fromMainActivity_search_results",resp);
                            startActivity(intent);
                        }

                        @Override
                        public void onFailure(String error) {
                            Toast.makeText(MainActivity.this, "Ocurrió un error", Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (NullPointerException e) {
                    Toast.makeText(MainActivity.this, "Búsqueda incorrecta", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "onClick - exception: " + e.getMessage());
                }


            }
        });
    }
}
