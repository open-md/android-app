package com.openmd.openmd;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {
    GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    public static boolean mLocationPermisionGranted;
    final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 0;
    private static final String TAG = "MapActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Retrieve the content view that renders the map.
        setContentView(R.layout.activity_map);

        // Get the SupportMapFragment and request notification
        // when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mLocationPermisionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermisionGranted = true;
                }
            }
        }
        updateLocationUI();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        String rawData = getIntent().getStringExtra("EXTRA_fromMainActivity_search_results");


        mMap = googleMap;
        updateLocationUI();

        LatLng barranco = new LatLng(-12.1404447,-77.0233722);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(barranco));
        mMap.moveCamera(CameraUpdateFactory.zoomTo(14));

        if (!rawData.equals("")) {
            setMarkers(rawData);
        }

        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                View v = getLayoutInflater().inflate(R.layout.view_marker_details, null);
                TextView establecimiento = v.findViewById(R.id.view_marker_details_estab);
                TextView direccion = v.findViewById(R.id.view_marker_details_direccion);
                TextView medicamentos = v.findViewById(R.id.view_marker_details_medicamentos);

                JSONObject elements = (JSONObject) marker.getTag();

                try {
                    establecimiento.setText(elements.getString("estab"));
                    direccion.setText(elements.getString("direccion"));
                    JSONArray medsInfo = elements.getJSONArray("contenido");
                    StringBuilder meds = new StringBuilder();
                    for(int i = 0; i < medsInfo.length(); i++) {
                        JSONObject x = medsInfo.getJSONObject(i);
                        meds.append(x.getString("medicamento"));
                        meds.append(" ");
                        meds.append(x.getString("presentacion"));
                        meds.append(" - Precio: S/ ");
                        meds.append(x.getString("monto_empaque"));
                        meds.append("\n");
                    }
                    medicamentos.setText(meds);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.d(TAG, "getInfoContents: " + elements);

                return v;
            }
        });

    }

    private void updateLocationUI() {
        if (mMap != null) {
            try {
                Log.d(TAG, "updateLocationUI: " + mLocationPermisionGranted);
                if (mLocationPermisionGranted) {
                    mMap.setMyLocationEnabled(true);
                    mMap.getUiSettings().setMyLocationButtonEnabled(true);
                } else {
                    mMap.setMyLocationEnabled(false);
                    mMap.getUiSettings().setMyLocationButtonEnabled(false);
                    getLocationPermission();
                }
            } catch (SecurityException e) {
                Log.e("Exception: %s", e.getMessage());
            }
        }
    }

    private void getLocationPermission() {
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLocationPermisionGranted = true;
            updateLocationUI();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    private void setMarkers(String rawData) {
        try {
            JSONObject rawJSONData = new JSONObject(rawData);
            Log.d(TAG, "setMarkers: - # markers: " + rawJSONData.length());

            for (int i = 0; i < rawJSONData.names().length(); i++) {
                String key = rawJSONData.names().getString(i);
                JSONObject element = rawJSONData.getJSONObject(key);

                final String estab = StringUtils.capitalize(element.getString("estab"));
                float lat = Float.parseFloat(element.getString("latitud"));
                float lng = Float.parseFloat(element.getString("longitud"));
                String direc = StringUtils.capitalize(key);

                element.put("direccion",key);
                mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng))).setTag(element);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(this, "No se encontraron resultados", Toast.LENGTH_SHORT).show();
        }
    }


}
