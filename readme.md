# Android App de Open MD
Usando esta aplicación, podrás encontrar los medicamentos genéricos más populares del Perú. Entre ellos:
* Diclofenaco: Reduce inflamaciones
* Sulfametoxazol/Trimetoprima: Antibiótico de uso general
* Clorfenamina: Alivia alergias de todo tipo
* Ampicilina: Antibiótico para infecciones bacterianas agudas
* Terramicina: Antifúngico
* Amoxicilina: Antibiótico para enfermedades pulmonares
* Penicilina/Bencilpenicilina: El más famoso antibiótico general
* Salbutamol: Medicamento para el asma
* Buscapina: Alivia dolores abdominales

Para buscar uno de estos medicamentos, ingresa su nombre en el buscador. Luego se te mostrará un mapa con marcadores, en donde podras ver el precio y las variaciones de este medicamento. Si haces click en el botón azul de abajo a la derecha, se te darán indicaciones para llegar a la farmacia en donde podrás encontrar este medicamento.
